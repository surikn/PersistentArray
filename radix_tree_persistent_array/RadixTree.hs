{-# LANGUAGE DeriveFunctor #-}
module Main where

import qualified Control.Exception as Exc
import           Control.Monad
import qualified Data.Maybe
import qualified Data.Vector       as V

-- @
-- stack ghc -- -threaded -rtsopts -O0 RadixTree.hs
-- ./RadixTree +RTS -h >/dev/null
--
-- eventlog2html -p --bands 40 RadixTree.hp
-- open RadixTree.hp.html
--
-- hp2ps -g -c RadixTree.hp
-- open RadixTree.ps
-- @
main :: IO ()
main = test1

-- TypeClass of RadixTree
-- If we define it recursively, it can be:
-- 1) [TODO] regular node whitch stores array of next nodes
-- 2) Leaf node
-- 3) Empty node
data RadixTree a =
    Empty
    --     (childs)                           (lvl)
    | Node Int (V.Vector (RadixTree a))
    | Leaf Int (V.Vector a)
    deriving (Show, Functor)

ex1 :: RadixTree Int
ex1 = dup4 (dup4 (dup4 (mkLeaf [1,2,3,4])))
  where
    dup4 x = mkNode [x, x, x, x]

completeTo :: Int -> a -> [a] -> [a]
completeTo n x xs = replicate m x ++ xs
  where
    m = n - length xs

height :: RadixTree a -> Int
height Empty       = 0
height (Node _ ts) = 1 + maximum (height <$> ts)
height (Leaf _ _)  = 1

getByIndex :: Int -> RadixTree a -> Maybe a
getByIndex i tree = go (completeTo (k * h) 0 (toBinary i)) tree
  where
    h = height tree
    k = 2
    go bs t =
      case t of
        Empty     -> Nothing
        Node _ ts -> do
          t' <- ts V.!? j
          go bs' t'
        Leaf _ xs -> xs V.!? j
      where
        bs' = drop k bs
        j = toDecimal (take k bs)

setAt :: Int -> a -> RadixTree a -> RadixTree a
setAt i new tree = go (completeTo (k * h) 0 (toBinary i)) tree
  where
    h = height tree
    k = 2
    go bs t =
      case t of
        Empty       -> Empty
        Node lvl ts   ->
          Node lvl (ts V.// [(j, go bs' (ts V.! j))])
        Leaf lvl xs ->
          Leaf lvl (xs V.// [(j, new)])
      where
        bs' = drop k bs
        j = toDecimal (take k bs)

copy :: RadixTree a -> RadixTree a
copy Empty         = Empty
copy (Node lvl ts) = Node lvl (copy <$> ts)
copy (Leaf lvl xs) = Leaf lvl xs

setAt' :: Int -> a -> RadixTree a -> RadixTree a
setAt' i new tree = go (completeTo (k * h) 0 (toBinary i)) tree
  where
    h = height tree
    k = 2
    go bs t =
      case t of
        Empty       -> Empty
        Node lvl ts   ->
          Node lvl (V.imap (\l t' -> if l == j then go bs' t' else copy t') ts)
        Leaf lvl xs ->
          Leaf lvl (xs V.// [(j, new)])
      where
        bs' = drop k bs
        j = toDecimal (take k bs)

test1 :: IO ()
test1 = do
  ts <- forM [1..100000] $ \i -> do
    let ex = setAt 23 i ex1
    print (getByIndex 23 ex)
    return ex
  forM_ ts $ \t -> do
    print (getByIndex 23 t)

  ts <- forM [1..100000] $ \i -> do
    let ex = setAt' 23 i ex1
    print (getByIndex 23 ex)
    return ex
  forM_ ts $ \t -> do
    print (getByIndex 23 t)


mkNode :: [RadixTree a] -> RadixTree a
mkNode ts = Node 0 (V.fromList (map incLevel ts))

mkLeaf :: [a] -> RadixTree a
mkLeaf xs = Leaf 0 (V.fromList xs)

incLevel :: RadixTree a -> RadixTree a
incLevel Empty       = Empty
incLevel (Node n ts) = Node (n+1) ts
incLevel (Leaf n xs) = Leaf (n+1) xs


-- -- Implementing map on RadixTree
-- radixTreeMap :: (a -> b) -> RadixTree a -> RadixTree b
-- radixTreeMap _ Empty             = Empty                                                   -- O(1)
-- radixTreeMap f (Leaf a lvl)      = Leaf(V.map f a) lvl                              -- O(1)
-- radixTreeMap f (Node childs lvl) = Node (V.map (radixTreeMap f) childs) lvl    -- O(N) if call call from root; O(1)
--

toBinary :: Int -> [Int]
toBinary 0 = [0]
toBinary n = go n []
    where go 0 r  = r
          go k rs = go (div k 2) (mod k 2:rs)


toDecimal :: [Int] -> Int
toDecimal bin = if null bin then 0 else 2^(length bin -1)* head bin + toDecimal (tail bin)

merge :: [a] -> [a] -> [a]
merge xs     []     = xs
merge []     ys     = ys
merge (x:xs) (y:ys) = x : y : merge xs ys

completeTo5 :: [Int] -> [Int]
completeTo5 arr = if mod (length arr) 5 == 0 then arr else completeTo5(merge [0] arr)


slice :: Int -> Int -> [a] -> [a]
slice from to xs = take (to - from + 1) (drop from xs)

localIndex :: Int -> Int -> Maybe Int
localIndex lvl ind
        | length bi < (lvl*5) = Just 0
        | otherwise = Just $ toDecimal (slice (5*lvl) (5*lvl+4) bi)
        where bi = completeTo5 (toBinary ind)


getRadixTreeVal :: RadixTree a -> Int -> Maybe a
getRadixTreeVal Empty ind = Nothing
getRadixTreeVal (Leaf lvl rt) ind
    | Just li <- indReq    = Just (rt V.! li)
    | otherwise            = Nothing
    where indReq = localIndex lvl ind
getRadixTreeVal (Node lvl childs) ind
    | Just li <- indReq    = getRadixTreeVal (childs V.! li) ind
    | otherwise            = Nothing
    where indReq = localIndex lvl ind




-- setRadixTreeVal :: RadixTree a -> Int -> a -> RadixTree a
-- setRadixTreeVal Empty ind val =
-- setRadixTreeVal (Leaf rt lvl) ind val
--     | Just li <- indReq    = (rt V.! li)
--     | otherwise            = rt    -- FIXME: THROW EXTENSION
--     where indReq = localIndex lvl ind
-- setRadixTreeVal (Node childs lvl) ind val
--     | Just li <- indReq    = setRadixTreeVal (childs V.! li) ind val
--     | otherwise            = childs -- FIXME: THROW EXCEPTION
--     where indReq = localIndex lvl ind

-- pushBackRadixTree :: RadixTree a -> Int -> ()
-- pushBackRadixTree Empty ind = ()


-- -- Making RadixTree Functor
-- instance Functor RadixTree where
--     fmap = radixTreeMap
--

