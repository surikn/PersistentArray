cmake_minimum_required(VERSION 3.19)
project(persistent_array)

set(CMAKE_CXX_STANDARD 20)

add_executable(persistent_array main.cpp)