#include "iostream"
#include "chrono"
#include "vector"
#include "numeric"
#include "random"
#include "memory"

using namespace std;

class BinaryTreePersistentArray {
public:
    struct Node {
        int _index = 0, value = 0, id = 0;
        shared_ptr<Node> left = nullptr, right = nullptr;
        Node() = default;
        Node(int i, int v, int u) {
            _index = i;
            value = v;
            id = u;
            left = right = nullptr;
        }
    };

    BinaryTreePersistentArray() {
        root = nullptr;
        detached = false;
    }

    explicit BinaryTreePersistentArray(const unsigned int &size) {
        this->size = size;
        detached = true;
        vector<int> ids(size - 1);
        iota(begin(ids), end(ids), 1);

        unsigned seed = chrono::system_clock::now().time_since_epoch().count();
        shuffle(begin(ids), end(ids), default_random_engine(seed));

        root = make_shared<Node>(0, 0, 0);
        for (int id : ids) {
            root = create(root, id);
        }
    }

    BinaryTreePersistentArray(const BinaryTreePersistentArray& other) {
        this->size = other.size;
        this->detached = false;
        this->root = other.root;
    }

    BinaryTreePersistentArray& operator=(const BinaryTreePersistentArray& other) {
        if (&other == this) return *this;
        this->detached = false;
        this->size = other.size;
        this->root = other.root;
        return *this;
    }

    void insert(int index, int value) {
        if (!detached) {
            root = setAndDetach(root, index, value);
            detached = true;
        } setValue(root, index, value);
    }
    int get(int idx) {
        return get(root, idx);
    }

private:
    unsigned int size{};
    bool detached;

    shared_ptr<Node> root;

    shared_ptr<Node> create(const shared_ptr<Node> &r, int index) {
        if (r == nullptr) return make_shared<Node>(index, 0, 0);
        if (index < r->_index) r->left = create(r->left, index);
        else r->right = create(r->right, index);
        return r;
    }

    void setValue(const shared_ptr<Node> &r, int index, int value) {
        if (r == nullptr) return;
        if (index == r->_index) r->value = value;
        else {
            if (index < r->_index) setValue(r->left, index, value);
            setValue(r->right, index, value);
        }
    }


    shared_ptr<Node> setAndDetach(const shared_ptr<Node> &r, int index, int value) {
        if (r == nullptr) return r;

        shared_ptr<Node> node = nullptr;
        if (index == r->_index) {
            node = make_shared<Node>(index, value, r->id + 1);
            node->left = r->left;
            node->right = r->right;
            return node;
        }

        if (index < r->_index) {
           auto l = setAndDetach(r->left, index, value);
            if (l != nullptr) {
                node = make_shared<Node>(r->_index, r->value, l->id);
                node->left = l;
                node->right = r->right;
            }
        } else {
            auto new_r = setAndDetach(r->right, index, value);
            if (r != nullptr) {
                node = make_shared<Node>(r->_index, r->value, new_r->id);
                node->left = r->left;
                node->right = r;
            }
        }
        return node;
    }

    int get(const shared_ptr<Node> &r, int index) {
        if (r == nullptr) return 0;
        if (index == r->_index) return r->value;
        if (index < r->_index) return get(r->left, index);
        else return get(r->right, index);
    }
};

int main() {
    BinaryTreePersistentArray persistentArray(10000);
    BinaryTreePersistentArray arrays[1000];
    for (int i = 0; i < 1000; i++) {
        arrays[i] = persistentArray;
        arrays[i].insert(i, i * i);
    }

    for (int i = 0; i < 1000; i++) {
        cout << arrays[i].get(i) << "\n";
    }

    return 0;
}